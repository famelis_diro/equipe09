package com.equipe09.Prototype;

public abstract class Compte {

	protected String prenom;
	protected String nom;
	protected String email;
	protected String telephone;
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	protected abstract void ajouter();
	protected abstract void modifier();
	protected abstract void supprimer();
}