package com.equipe09.Prototype;

/*
 * Controlleur pour Consulter les inscriptions à la 
 * (aux) séance(s) pour un professionnel (CU #5)
 * */

public class ConsulterInscriptionsSeance {
	
	public static void afficherListeServicesOfferts(String numeroProfessionnel) {
		/*Afficher la liste des services offerts par un professionnel*/
		if( numeroProfessionnel.length()==9 ) {
			System.out.print("Voici la liste des cours offerts par le professionnel\n\n");
			System.out.println("1234567 - POWER CARDIO\n3458394 - GOFITNESS MUSCLES\n9847348 - NEWBODY\n ");
		}else {
			System.out.println("Le numéro entré ne contient pas 9 chiffres.");
		}	
	}
	
	public static void afficherInscriptionDetail(String codeService) {
		if(codeService.length()==7) {
			System.out.println("\nVoici les inscriptions au service:\n"
					+ "Marilyn W. Yanez\n" + 
					"Rocio Rau\nPhilippe Tullie\nMarie Toupin\nSarah Desjardins\n ");
		}else {
			System.out.println("Le code entré ne correspond à aucun service.");
		}
			
	}
}
