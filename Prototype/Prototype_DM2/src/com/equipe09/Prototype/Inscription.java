package com.equipe09.Prototype;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Inscription {
	protected LocalDateTime dateHeureInscription;
	protected LocalDate dateService;
	protected int numeroProfessionnel;
	protected int numeroMembre;
	protected int codeService;
	protected String commentaire;
	
	public LocalDateTime getDateHeureInscription() {
		return dateHeureInscription;
	}
	public void setDateHeureInscription(LocalDateTime dateHeureInscription) {
		this.dateHeureInscription = dateHeureInscription;
	}
	public LocalDate getDateService() {
		return dateService;
	}
	public void setDateService(LocalDate dateService) {
		this.dateService = dateService;
	}
	public int getNumeroProfessionnel() {
		return numeroProfessionnel;
	}
	public void setNumeroProfessionnel(int numeroProfessionnel) throws Exception {
		if(numeroProfessionnel < 0 || numeroProfessionnel >= 1000000000)
			throw new Exception("Le numéro du professionnel doit être à 9 chiffres.");
		this.numeroProfessionnel = numeroProfessionnel;
	}
	public int getNumeroMembre() {
		return numeroMembre;
	}
	public void setNumeroMembre(int numeroMembre) throws Exception {
		if(numeroProfessionnel < 0 || numeroProfessionnel >= 1000000000)
			throw new Exception("Le numéro du membre doit être à 9 chiffres.");
		this.numeroMembre = numeroMembre;
	}
	public int getCodeService() {
		return codeService;
	}
	public void setCodeService(int codeService) throws Exception {
		if(codeService < 0 || codeService >= 10000000)
			throw new Exception("Le code du Service doit être à 7 chiffres.");
		this.codeService = codeService;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) throws Exception {
		if(commentaire == null || commentaire.length() > 100)
			throw new Exception("Le commentaire est de 100 caractères maximum.");
		this.commentaire = commentaire;
	}
	
	public Inscription() { }
	
	public void ajouter() {
		/*Logique d'ajout d'un inscription*/
	}
}
