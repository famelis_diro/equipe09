##IFT2255|A 2018 – DEVOIR MAISON 2 – LOGICIEL #GYM – INCRÉMENT 2 : ÉLABORATION
- Modèle de développement: Processus itératif et incrémental.
- DM2:
    - Précisition et ajout de certains besoins.
    - Modifications des cas d'utilisation produits au DM1.
    - Concentration sur le workflow d'analyse et de conception.
    - But: Pratique de l'analyse des exigences, conception d'un logiciel et construction de diagrammes d'activité.
