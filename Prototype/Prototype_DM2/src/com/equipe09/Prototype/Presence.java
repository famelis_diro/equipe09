package com.equipe09.Prototype;

import java.time.LocalDate;

public class Presence {
	protected LocalDate dateHeureCreation;
	protected int numeroProfessionnel;
	protected int numeroMembre;
	protected int codeService;
	protected String commentaire;
	
	public LocalDate getDateHeureCreation() {
		return dateHeureCreation;
	}

	public void setDateHeureCreation(LocalDate dateHeureCreation) {
		this.dateHeureCreation = dateHeureCreation;
	}

	public int getNumeroProfessionnel() {
		return numeroProfessionnel;
	}

	public void setNumeroProfessionnel(int numeroProfessionnel) {
		this.numeroProfessionnel = numeroProfessionnel;
	}

	public int getNumeroMembre() {
		return numeroMembre;
	}

	public void setNumeroMembre(int numeroMembre) {
		this.numeroMembre = numeroMembre;
	}

	public int getCodeService() {
		return codeService;
	}

	public void setCodeService(int codeService) {
		this.codeService = codeService;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
	public Presence() { }
	
	public void ajouter() {
		/*Ajoute la présence dans le  Répertoire des Services*/
	}
}
