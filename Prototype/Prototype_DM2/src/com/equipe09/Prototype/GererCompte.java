package com.equipe09.Prototype;

/*Controlleur pour Gérer Compte (CU #6)*/

public class GererCompte {
	
	
	public static void ajouter(char typeCompte, String nom, String prenom, String email, String telephone) {
		Compte c;
		try {
			
			if(Character.toLowerCase(typeCompte) == 'm') {
				Membre m = new Membre();
				System.out.println("Veuillez demander au nouveau membre de payer les frais d'adhésion. Appuyer sur une touche pour continuer...\n");
				System.in.read();
				m.setEstSuspendu(false);
				c = m;
			}
			else if(Character.toLowerCase(typeCompte) == 'p') {
				c = new Professionnel();
			}
			else {
				throw new Exception("Type de Compte non-supporté");
			}
			c.setEmail(email);
			c.setNom(nom);
			c.setPrenom(prenom);
			c.setTelephone(telephone);
			
			/*Ajouter le compte et obtenir le numéro du compte*/
			c.ajouter();
			
			System.out.println("Le compte #654987564 a été créé dans le Répertoire des Comptes avec succès.\n");
		}
		catch(Exception e) {
			System.out.println("Le compte n'a pas été créé dans le Répertoire des Comptes.\n");
		}

	}
	
	public static void modifier(String numeroCompte, String nomChamps, String valeur) {
		try {
			if(numeroCompte.length() != 9)
				throw new Exception("Le code du service doit être de longueur 9");
			int numero = Integer.parseInt(numeroCompte);
			Compte c = new Membre();/*Remplacer new Membre() par le Membre ou Professionnel associé au numeroCompte*/
			if(nomChamps == "prenom") {
				c.setPrenom(valeur);
			}
			else if(nomChamps == "nom") {
				c.setNom(valeur);
			}
			else if(nomChamps == "email") {
				c.setEmail(valeur);
			}
			else if(nomChamps == "telephone") {
				c.setTelephone(valeur);
			}
			else if(nomChamps == "estSuspendu") {
				if(c instanceof Membre) {
					if(valeur == "0") {
						((Membre)c).setEstSuspendu(false);
					}
					else if (valeur == "1") {
						((Membre)c).setEstSuspendu(true);
					}
					else {
						throw new Exception("Valeur pour estSuspendu invalide.");
					}
				}
				else {
					throw new Exception("Seulement le membre a un champs estSuspendu");
				}
			}
			c.modifier();
			System.out.println("Le champs "+ nomChamps + " a été modifié avec succès pour le compte #" + numeroCompte + " dans le Répertoire des Comptes.\n");
		}
		catch(Exception e) {
			System.out.println("Le champs "+ nomChamps + " n'a été pas modifié pour le compte #" + numeroCompte + " dans le Répertoire des Comptes.\n");
		}
	}
	
	public static void supprimer(String numeroCompte) {
		try {
			if(numeroCompte.length() != 9)
				throw new Exception("Le code du service doit être de longueur 9.");
			int numero = Integer.parseInt(numeroCompte);
			Compte c = new Membre();/*Remplacer new Membre() par le Membre ou Professionnel associé au numeroCompte*/
			c.supprimer();
			System.out.println("Le compte #" + numeroCompte + " a été supprimé du Répertoire des Comptes avec succès.\n");
		}
		catch(Exception e) {
			System.out.println("Le compte #" + numeroCompte + " n'a pas été supprimé du Répertoire des Comptes.\n");
		}
	}
}
