package com.equipe09.Prototype;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/*Controlleur pour Gérer Service (CU#4)*/

public class GererService {	
	/*
	 *  String dateHeureActuelles; //Date et heure actuelles (JJ-MM-AAAA HH:MM:SS)
	 String dateDebut; //Date de début du service (JJ-MM-AAAA)
	 String dateFin; //Date de fin du service (JJ-MM-AAAA)
	 String heure; //Heure du service (HH:MM)
	 String [] recurrenceHedomadaire; //Récurrence hebdomadaire du service (quels jours il est offert à la même heure)
	 int capaciteMax; //Capacité maximale (maximum 30 inscriptions)
	 String numProf; //Numéro du professionnel (9 chiffres)
	 String numService; //Code du service (7 chiffres)
	 double frais; //Frais du service (jusqu'à 100.00$)
	 String commentaires; //Commentaires (100 caractères) (facultatif)
	 * 
	 * */
	public static void ajouter(String dateDebut, String dateFin, String heure, 
			String recurrenceHedomadaire, String capaciteMaximale, 
			String numeroProfessionnel, String frais, String commentaire) {

		LocalDate dateDebutValide;
		LocalDate dateFinValide;
		LocalTime heureValide;
		String[] recurrenceHebdomadaireValide;
		int capaciteMaximaleValide;
		int numeroProfessionnelValide;
		double fraisValide;
		try {
			 dateDebutValide = CentreDeDonnee.stringToLocalDate(dateDebut, "dd-MM-uuuu");
			 dateFinValide = CentreDeDonnee.stringToLocalDate(dateFin, "dd-MM-uuuu");
			 heureValide = CentreDeDonnee.stringToLocalTime(heure, "kk:mm");
			 recurrenceHebdomadaireValide = recurrenceHedomadaire.split("\\s+");
			 capaciteMaximaleValide = Integer.parseInt(capaciteMaximale);
			 numeroProfessionnelValide = Integer.parseInt(numeroProfessionnel);
			 fraisValide = Double.parseDouble(frais);
			 
			 Service s = new Service();
			 s.setDateDebut(dateDebutValide);
			 s.setDateFin(dateFinValide);
			 s.setHeure(heureValide);
			 s.setRecurrenceHedomadaire(recurrenceHebdomadaireValide);
			 s.setCapaciteMaximal(capaciteMaximaleValide);
			 s.setNumeroProfessionnel(numeroProfessionnelValide);
			 s.setFrais(fraisValide);
			 s.setCommentaires(commentaire);
			 s.setDateHeureActuelles(LocalDateTime.now());
			 
			 /*Sauvegarder le service sur le disque et afficher le code du Service*/
			 s.ajouter();
			 
			 System.out.println("Le Service #3216878 a été ajouté au Répertoire des Services avec succès.\n");
		}
		catch(Exception e) {
			System.out.println("Le Service n'a pas été ajouté au Répertoire des Services.\n");
		}
	}
	
	public static void modifier(String codeService, String nomChamps, String valeur) {
		String champ = nomChamps.toLowerCase();
		try {
			if(codeService.length() != 7)
				throw new Exception("Le code du service doit être de longueur 7.");
			int code = Integer.parseInt(codeService);
			Service s = new Service();/*Remplacer par récupérer le service avec le #code*/
			
			if(champ == "datedebut") {
				s.setDateDebut(CentreDeDonnee.stringToLocalDate(valeur, "dd-MM-uuuu"));
			}
			else if(champ == "datefin") {
				s.setDateFin(CentreDeDonnee.stringToLocalDate(valeur, "dd-MM-uuuu"));
			}	
			else if(champ == "heure") {
				s.setHeure(CentreDeDonnee.stringToLocalTime(valeur, "kk:mm"));
			}
			else if (champ == "recurrence") {
				s.setRecurrenceHedomadaire(valeur.split("\\s+"));
			}
			else if(champ == "capacite") {
				s.setCapaciteMaximal(Integer.parseInt(valeur));
			}
			else if(champ == "nump") {
				s.setNumeroProfessionnel(Integer.parseInt(valeur));
			}
			else if(champ == "frais") {
				s.setFrais(Double.parseDouble(valeur));
			}
			else if(champ == "commentaire") {
				s.setCommentaires(valeur);
			}
			else {
				throw new Exception("Champ Invalide");
			}
			s.modifier();
			System.out.println("Le Service #"+ codeService +" a été modifié dans le Répertoire des Services avec succès.\n");
		}
		catch(Exception e) {
			System.out.println("Le Service #"+ codeService +" n'a pas été modifié dans le Répertoire des Services.\n");
		}
	}
	
	public static void supprimer(String codeService) {
		try {
			if(codeService.length() != 7)
				throw new Exception("Le code du service doit être de longueur 7.");
			int code = Integer.parseInt(codeService);
			Service s = new Service();/*Remplacer par récupérer le service avec le #code*/
			s.supprimer();
			System.out.println("Le Service #"+ codeService +" a été supprimé dans le Répertoire des Services avec succès.\n");
		}
		catch(Exception e) {
			System.out.println("Le Service #"+ codeService +" n'a pas été supprimé dans le Répertoire des Services.\n");
		}
	}
}
