package com.equipe09.Prototype;
import java.util.*;

/*Controlleur pour Consulter le Répertoire de Service (CU #8)*/

public class ConsulterSeance {
	public static void afficherListeSeance() {
		System.out.println("***Répertoire des Services***\n");
		System.out.println("1234567 - POWER CARDIO\n3458394 - GOFITNESS MUSCLES\n9847348 - NEWBODY\n9085687 - GOFITNESS FLEX\n8908909 - GOFITNESS ZEN YOGA\n"
				+ "1231234 - BODYFLOW\n2345456 - YOGA SUÉDOIS\n");
	}
	
	public static void afficherDetailSeance(String codeService) {
		Date d = new Date();
		if(codeService.length()==7){
			System.out.println("\nDate et heure actuelles : "+d
					+"\nDate du service : 20-11-2018"
					+ "\nNuméro du professionnel : 123456789"
					+ "\nNuméro du service : 1234567"
					+ "\nCapacité maximale : 15"
					+ "\nFrais : 75.00$");
			
		}else {
			System.out.println("Veuillez entrer un code de service valide.");
		}
	}
}
