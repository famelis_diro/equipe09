package com.equipe09.Prototype;

public class Membre extends Compte {
	protected int numeroMembre;
	protected boolean estSuspendu;
	
	public boolean getEstSuspendu() {
		return estSuspendu;
	}

	public void setEstSuspendu(boolean estSuspendu) {
		this.estSuspendu = estSuspendu;
	}

	public int getNumeroMembre() {
		return numeroMembre;
	}

	public void setNumeroMembre(int numeroMembre) {
		this.numeroMembre = numeroMembre;
	}

	@Override
	protected void ajouter() {
		// Ajouter un membre dans le Répertoire des Comptes
		
	}

	@Override
	protected void modifier() {
		// Modifier un membre dans le Répertoire des Comptes
		
	}

	@Override
	protected void supprimer() {
		// Supprimer un membre dans le Répertoire des Comptes
		
	}
}
