package com.equipe09.Prototype;

public class Professionnel extends Compte {
	protected int numeroProfessionnel;
	
	public int getNumeroProfessionnel() {
		return numeroProfessionnel;
	}

	public void setNumeroProfessionnel(int numeroProfessionnel) {
		this.numeroProfessionnel = numeroProfessionnel;
	}

	@Override
	protected void ajouter() {
		// Ajouter un membre dans le Répertoire des Comptes
		
	}

	@Override
	protected void modifier() {
		// Modifier un membre dans le Répertoire des Comptes
		
	}

	@Override
	protected void supprimer() {
		// Supprimer Auto-generated method stub
		
	}
	
	public Professionnel() { }
}
