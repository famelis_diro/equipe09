package com.equipe09.Prototype;

import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Service {
	//Attibutes
	protected LocalDateTime dateHeureActuelles; //Date et heure actuelles (JJ-MM-AAAA HH:MM:SS)
	protected LocalDate dateDebut; //Date de début du service (JJ-MM-AAAA)
	protected LocalDate dateFin; //Date de fin du service (JJ-MM-AAAA)
	protected LocalTime heure; //Heure du service (HH:MM)
	protected String[] recurrenceHedomadaire; //Récurrence hebdomadaire du service (quels jours il est offert à la même heure)
	protected int capaciteMaximal; //Capacité maximale (maximum 30 inscriptions)
	protected int numeroProfessionnel; //Numéro du professionnel (9 chiffres)
	protected int codeService; //Code du service (7 chiffres)
	protected double frais; //Frais du service (jusqu'à 100.00$)
	protected String commentaires; //Commentaires (100 caractères) (facultatif)
	
	//Properties
	public LocalDateTime getDateHeureActuelles() {
		return dateHeureActuelles;
	}

	public void setDateHeureActuelles(LocalDateTime dateHeureActuelles) {
		this.dateHeureActuelles = dateHeureActuelles;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebutValide) {
		this.dateDebut = dateDebutValide;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) throws Exception {
		if (this.dateDebut != null && this.dateDebut.isAfter(dateFin))
			throw new Exception("Date Fin doit être après Date Début.");
		this.dateFin = dateFin;
	}

	public LocalTime getHeure() {
		return heure;
	}

	public void setHeure(LocalTime heure) {
		this.heure = heure;
	}

	public String[] getRecurrenceHedomadaire() {
		return recurrenceHedomadaire;
	}

	public void setRecurrenceHedomadaire(String[] recurrenceHedomadaire) {
		this.recurrenceHedomadaire = recurrenceHedomadaire;
	}

	public int getCapaciteMaximal() {
		return capaciteMaximal;
	}

	public void setCapaciteMaximal(int capaciteMaximal) throws Exception {
		if(capaciteMaximal < 0 || capaciteMaximal > 30)
			throw new Exception("La capacité maximale d'un service est de 30 participants.");
		this.capaciteMaximal = capaciteMaximal;
	}

	public int getNumeroProfessionnel() {
		return numeroProfessionnel;
	}

	public void setNumeroProfessionnel(int numeroProfessionnel) throws Exception {
		if(numeroProfessionnel < 0 || numeroProfessionnel >= 1000000000)
			throw new Exception("Le numéro du professionnel doit être à 9 chiffres.");
		this.numeroProfessionnel = numeroProfessionnel;
	}

	public int getCodeService() {
		return codeService;
	}

	public void setCodeService(int codeService) throws Exception {
		if(codeService < 0 || codeService >= 10000000)
			throw new Exception("Le code du Service doit être à 7 chiffres.");
		this.codeService = codeService;
	}

	public double getFrais() {
		return frais;
	}

	public void setFrais(double frais) throws Exception {
		if(frais < 0 || frais > 100)
			throw new Exception("Les frais d'un service doivent être au plus 100$.");
		this.frais = frais;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) throws Exception {
		if(commentaires == null || commentaires.length() > 100)
			throw new Exception("Le commentaire est de 100 caractères maximum.");
		this.commentaires = commentaires;
	}

	//Constructeur
	public Service(){}
	
	//Methods
	public void ajouter() {
		/*Ajouter le service dans le Répertoire des Services*/
	}
	public void modifier() {
		/*Modifier le service dans le Répertoire des Services*/
	}
	public void supprimer() {
		/*Supprimer le service dans le Répertoire des Services*/
	}
}


