package com.equipe09.Prototype;

/*Controlleur pour confirmer la présence d'un membre (CU #9)*/

public class ConfirmerPresence {
	
	public static void confirmerPresence(String numeroMembre, Service service) {
		boolean hasErrors = numeroMembre.length() != 9;
		int numero = 0;
		try {
			numero = Integer.parseInt(numeroMembre);
		}
		catch(NumberFormatException e) {
			hasErrors = true;
		}
		
		if(hasErrors)
		{
			System.out.println("Numéro Invalide");
			return;
		}
		
		/*Numéro est valide, donc trouver membre/professionnel valide*/
		/*Pour le prototype, si le numéro est valide, le membre est toujours valide, 
		 * sauf pour le numéro 123456789 qui lui est toujours suspendu*/
		if (numero == 123456789)
		{	
			System.out.println("Le membre est suspendu puisque ses frais d'adhésion n'ont pas été payé");
		}
		else {
			/* Créer l'enregistrement sur le disque*/
			
			System.out.println("La présence du membre a été confirmé");
		}
	}
}
