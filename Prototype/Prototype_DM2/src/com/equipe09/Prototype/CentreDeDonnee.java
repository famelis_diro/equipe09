package com.equipe09.Prototype;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.Temporal;
import java.util.Scanner;

public class CentreDeDonnee {
	
	public static void main(String[] args) {
		while(true) {
			 afficherTextAccueil();
		}
	}
 
	public static void afficherTextAccueil()
	{	
	    System.out.println("******Bienvenue dans le logiciel #GYM******\n");
	    System.out.println("Veuillez vous identifier.\n");
	    System.out.println("1.Vous êtes un Agent.\n");
	    System.out.println("2.Vous êtes un Gerant.\n");
	    System.out.println("3.Quitter\n");
	    
	 	/*Valider l'option*/
	    switch(choixMenu()) {
	    	case 1:
	    		affichageTextOptionAgent();
	    		break;
	    	case 2:
	    		affichageTextOptionGerant();
	    		break;
	    	case 3:
	    		System.exit(0);
	    	default:
	    		System.out.println("Option invalide");
	    		break;
	    }
	}

	public static void affichageTextOptionAgent() {
		System.out.println("****** En Mode Agent ******\n");
	 	System.out.println("Veuillez Choisir une option.\n");
	 	System.out.println("1.Identifier Membre.\n");
	 	System.out.println("2.Gestion Compte.\n");
	 	System.out.println("3.Gestion service.\n");
	 	System.out.println("4.Consulter la liste des séances.\n");
	 	System.out.println("5.Consulter les inscriptions à une séance.\n");
	 	System.out.println("6.Inscrire un membre à une séance.\n");
	 	System.out.println("7.Quitter\n");
	 	
	 	/*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		affichageIdentifierMembre();
	    		break;
	    	case 2:
	    		afficherGererCompte();
	    		break;
	    	case 3:
	    		afficherGererService();
	    		break;
	    	case 4:
	    		afficherConsulterListeSeance();
	    		break;
	    	case 5:
	    		afficherConsulterInscriptionsSeances();
	    		break;
	    	case 6: //Inscrire un membre a une séance
	    		afficherdonneeInscriptionServices();
	    	case 7:
	    		/*Quitter*/
	    		break;
	    	default:
	    		System.out.println("Option invalide");
	    		break;
	 	}
 	}
	
	public static void afficherConsulterListeSeance() {
		System.out.println("****** En Mode Agent ******\n");
	 	System.out.println("Veuillez Choisir une option.\n");
	 	System.out.println("1.Afficher la liste des séances.\n");
	 	System.out.println("2.Afficher le détail d'une séance.\n");
	 	System.out.println("3.Quitter\n");
	 	
	 	/*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		ConsulterSeance.afficherListeSeance();
	    		break;
	    	case 2:
	    		System.out.println("Veuillez entrer le code du Service.\n");
	    		ConsulterSeance.afficherDetailSeance(entreeReponseClavier());
	    		break;
	    	case 3:
	    		/*Quitter*/
	    		return;
	    	default:
	    		System.out.println("Option invalide");
	    		break;
	 	}
		afficherConsulterListeSeance();
	}
	
	public static void afficherConsulterInscriptionsSeances() {
		System.out.println("****** En Mode Agent ******\n");
	 	System.out.println("Veuillez Choisir une option.\n");
	 	System.out.println("1.Afficher la liste des inscriptions d'un professionnel.\n");
	 	System.out.println("2.Afficher le détail des inscriptions d'un service.\n");
	 	System.out.println("3.Quitter\n");

	 	/*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		System.out.println("Veuillez entrer le numéro du professionnel.");
	    		String numeroProfessionnel = entreeReponseClavier();
	    		System.out.println("\n\n");
	    		ConsulterInscriptionsSeance.afficherListeServicesOfferts(numeroProfessionnel);
	    		break;
	    	case 2:
	    		System.out.println("Veuillez choisir et entrer le code du Service.");
	    		String codeService = entreeReponseClavier();
	    		ConsulterInscriptionsSeance.afficherInscriptionDetail(codeService);
	    		break;
	    	case 3:
	    		/*Quitter*/
	    		return;
	    	default:
	    		System.out.println("Option invalide");
	    		break;
	 	}
	 	afficherConsulterInscriptionsSeances();
	}
	
	public static void affichageTextOptionGerant() {
	    System.out.println("****** En Mode Gérant ******\n");
	    System.out.println("Veuillez Choisir une option.\n");
	    System.out.println("1.Exécuter le rapport de Synthèse pour la semaine.\n");
	    System.out.println("2.Quitter\n");
	    
	    /*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		ProcedureComptable.ProcedureComptable(true);
	    		break;
	    	case 2:
	    		/*Quitter*/
	    		break;
	    	default:
	    		System.out.println("Option invalide");
	    		afficherTextAccueil();
	    		break;
	 	}
	}
	
 	
	public static void affichageIdentifierMembre() {
		System.out.println("****** En Mode Agent ******\n");
		System.out.println("Veuillez Entrer le numéro de membre.\n");	
		IdentifierCompte.identifierCompte(entreeReponseClavier());
	}
	
	public static void afficherGererService() {
		System.out.println("****** En Mode Agent ******\n");
	 	System.out.println("Veuillez Choisir une option.\n");
	 	System.out.println("1.Ajouter un Service.\n");	
	 	System.out.println("2.Modifier un Service.\n");	
	 	System.out.println("3.Supprimer un Service.\n");	
	 	System.out.println("4.Quitter\n");
	 	
	 	/*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		System.out.println("Veuillez entrer la date de début du Service avec le format (JJ-MM-AAAA)");
	    		String dateDebut = entreeReponseClavier();
	    		System.out.println("Veuillez entrer la date de fin du Service avec le format (JJ-MM-AAAA)");
	    		String dateFin = entreeReponseClavier();
	    		System.out.println("Veuillez entrer l'heure du Service (HH:SS)");
	    		String heure = entreeReponseClavier();
	    		System.out.println("Veuillez entrer les jours auxquels le Service sera donné. Séparé par une (des) espace(s) "
	    				+ "s'il y en a plus de un. Valeurs possible lundi mardi mercredi jeudi vendredi samedi dimanche");
	    		String recurrenceHedomadaire = entreeReponseClavier();
	    		System.out.println("Veuillez entrer la capacité maximale du Service (Maximum 30 participants)");
	    		String capaciteMaximal = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le numéro du profesionnel (9 chiffres)");
	    		String numeroProfessionnel = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le coût du Service (entre 0.00 et 100.00");
	    		String frais = entreeReponseClavier();
	    		System.out.println("Veuillez entrer un commentaire (facultatif, maximum 100 caractères)");
	    		String commentaire = entreeReponseClavier();
	    		GererService.ajouter(dateDebut, dateFin, heure, recurrenceHedomadaire, 
	    				capaciteMaximal, numeroProfessionnel, frais, commentaire);
	    		break;
	    	case 2:
	    		System.out.println("Veuillez entrer le code du Service qu'il faut modifier (7 chiffres)");
	    		String codeService = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le champs qu'il faut modifier.Voici la liste des options possibles en ignorant les << et >>\n");
	    		System.out.println("<<datedebut>> pour changer la date de début du service (JJ-MM-AAAA)");
	    		System.out.println("<<datefin>> pour changer la date de fin du service (JJ-MM-AAAA)");
	    		System.out.println("<<heure>> pour changer l'heure de début du service (HH:MM)");
	    		System.out.println("<<recurence>> pour changer la récurrence hebdomadaire du service. Valeurs possible lundi mardi mercredi jeudi vendredi samedi dimanche");
	    		System.out.println("<<capacite>> pour changer le nombre de participants maximal au service (maximum 30)");
	    		System.out.println("<<nump>> pour changer le numéro du professionnel associé à ce service (9 chiffres)");
	    		System.out.println("<<frais>> pour changer les frais associé à ce service (maximum 100.00)");
	    		System.out.println("<<commentaire>> pour changer le commentaire associé à ce service (maximum 100 caractères)");
	    		String nomChamps = entreeReponseClavier();
	    		System.out.println("Veuillez entrer la nouvelle valeur pour " + nomChamps);
	    		String valeur = entreeReponseClavier();
	    		GererService.modifier(codeService, nomChamps, valeur);
	    		break;
	    	case 3:
	    		System.out.println("Veuillez entrer le code du Service qu'il faut modifier (7 chiffres)");
	    		String code = entreeReponseClavier();
	    		GererService.supprimer(code);
	    		break;
	    	case 4:
	    		/*Quitter*/
	    		break;
	    	default:
	    		System.out.println("Option invalide");
	    		afficherGererService();
	    		break;
	 	}
	}
	
	public static void afficherGererCompte() {
		System.out.println("****** En Mode Agent ******\n");
	 	System.out.println("Veuillez Choisir une option.\n");
	 	System.out.println("1.Ajouter un Compte.\n");	
	 	System.out.println("2.Modifier un Compte.\n");	
	 	System.out.println("3.Supprimer un Compte.\n");	
	 	System.out.println("4.Quitter\n");
	 	
	 	/*Valider l'option*/
	 	switch(choixMenu()) {
	    	case 1:
	    		System.out.println("Veuillez entrer le type de compte que vous voulez créer (m pour membre et p pour professionnel)");
	    		char typeCompte = entreeReponseClavier().charAt(0);
	    		System.out.println("Veuillez entrer le prénom");
	    		String prenom = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le nom");
	    		String nom = entreeReponseClavier();
	    		System.out.println("Veuillez entrer l'email");
	    		String email = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le téléphone");
	    		String telephone = entreeReponseClavier();
	    		GererCompte.ajouter(typeCompte, nom, prenom, email, telephone);
	    		break;
	    	case 2:
	    		System.out.println("Veuillez entrer le numéro du compte qu'il faut modifier (7 chiffres)");
	    		String numeroCompte = entreeReponseClavier();
	    		System.out.println("Veuillez entrer le champs qu'il faut modifier.Voici la liste des options possibles en ignorant les << et >>\n");
	    		System.out.println("<<prenom>> pour changer le nom associé à ce compte");
	    		System.out.println("<<nom>> pour changer le prénom associé à ce compte");
	    		System.out.println("<<email>> pour changer l'email associé à ce compte");
	    		System.out.println("<<telephone>> pour changer le téléphone associé à ce compte");
	    		System.out.println("<<estSuspendu>> pour changer le statut des frais d'adhésion du membre");
	    		String nomChamps = entreeReponseClavier();
	    		System.out.println("Veuillez entrer la nouvelle valeur pour " + nomChamps);
	    		String valeur = entreeReponseClavier();
	    		GererCompte.modifier(numeroCompte, nomChamps, valeur);
	    		break;
	    	case 3:
	    		System.out.println("Veuillez entrer le numéro du compte qu'il faut modifier (7 chiffres)");
	    		String numeroCompteSuppression = entreeReponseClavier();
	    		GererCompte.supprimer(numeroCompteSuppression);
	    		break;
	    	case 4:
	    		/*Quitter*/
	    		break;
	    	default:
	    		System.out.println("Option invalide");
	    		afficherGererCompte();
	    		break;
	 	}
	}
	public static void afficherdonneeInscriptionServices() {
		/* [0] Date du service (JJ-MM-AAAA)
		 * [1] Numéro du Professionnel (9 chiffres)
		 * [2] Numéro du Membre (9 chiffres)
		 * [3] Code du Service (7 chiffres)
		 * [4] Commentaires (100 caractères) (facultatif)
		 * */
		System.out.println("Veuillez entrer la date du Service (JJ-MM-AAAA)");
		String dateService = entreeReponseClavier();
		System.out.println("Veuillez entrer le numéro du profesionnel (9 chiffres)");
		String numeroProfessionnel = entreeReponseClavier();
		System.out.println("Veuillez entrer le numéro du Membre (9 chiffres)");
		String numeroMembre = entreeReponseClavier();
		System.out.println("Veuillez entrer le code du service (7 chiffres)");
		String codeService = entreeReponseClavier();
		System.out.println("Veuillez entrer un commentaire (facultatif, maximum 100 caractères)");
		String commentaire = entreeReponseClavier();
		InscriptionSeance.inscriptionSeance(dateService,numeroProfessionnel,numeroMembre,codeService,commentaire);
		
		
	}
	// scann ce qu'entre l'utilisateur dans la console comme choix d'option
	
	public static int choixMenu(){  
		Scanner scan = new Scanner(System.in);
		while(!scan.hasNextInt())
		{
			scan.nextLine();
		}
		int option = scan.nextInt(); 
		return option;
	}
	
	// scann ce qu'entre l'agent  dans la console comme information
	
	public static String entreeReponseClavier(){ 
		Scanner scan = new Scanner(System.in);
		String action = scan.nextLine();
		while(action.equals(""))
		{
			action = scan.nextLine();
		}
		
		return action;
	}
	
	public static LocalDate stringToLocalDate(String date, String format) throws DateTimeParseException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		return LocalDate.parse(date, dtf);
	}
	
	public static LocalTime stringToLocalTime(String date, String format) throws DateTimeParseException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		return LocalTime.parse(date, dtf);
	}
	
	public static String dateToString(Temporal ld, String format) throws DateTimeException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
		return dtf.format(ld);
	}
}
