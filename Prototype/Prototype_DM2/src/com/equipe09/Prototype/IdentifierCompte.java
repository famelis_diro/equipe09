package com.equipe09.Prototype;

/*Controlleur pour identifier un compte (CU#10)*/

public class IdentifierCompte {
	
	
	public static void identifierCompte(String numeroCompte) {
		boolean hasErrors = numeroCompte.length() != 9;

		int numero = 0;
		try {
			numero = Integer.parseInt(numeroCompte);
		}
		catch(NumberFormatException e) {
			hasErrors = true;
		}
		
		if(hasErrors)
		{
			System.out.println("Numéro Invalide \n");
			return;
		}
		
		/*Num�ro est valide, donc trouver membre/professionnel valide*/
		/*Pour le prototype, si le numéro est valide, le membre est toujours valide, 
		 * sauf pour le numéro 123456789 qui lui est toujours suspendu*/
		if (numero == 123456789)
		{	
			System.out.println("Le membre est suspendu puisque ses frais d'adhésion n'ont pas été payé.\n");
		}
		else {
			/*TODO : Identifier le compte en utlisant le répertoire des Comptes*/
			System.out.println("Numéro Valide");
		}
	}
}
