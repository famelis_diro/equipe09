package com.equipe09.Prototype;

/*Controlleur pour Procedure comptable (CU #3) et les rapports associés (CU #1 et CU #2)*/

public class ProcedureComptable {
	/*Cette méthode est exécutée à vendredi minuit toutes les semaines*/
	public static void ProcedureComptable() {
		ProcedureComptable(false);
		System.out.println("La Procédure Comptable s'est terminée avec succès.\n");
	}
	
	public static void ProcedureComptable(boolean seulementSynthese) {
		if(!seulementSynthese)
			genererFichierTEF();
		genererRapportSynthese();
	}
	
	private static void genererFichierTEF() {
		/*Générer les données pour le fichier TEF*/
		/*Générer le fichier TEF*/
		/*Sauvegarder le fichier TEF sur le disque*/
		System.out.println("Le fichier TEF est prêt pour la consultation et/ou l'envoi.\n");
	}
	private static void genererRapportSynthese() {
		/*Générer les données pour le rapport de Synthèse*/
		/*Calculer les totaux*/
		/*Générer le rapport de Synthèse*/
		/*Sauvegarder le fichier TEF sur le disque*/
		System.out.println("Le Rapport de Synthèse est prêt pour la consultation et/ou l'envoi.\n");
	}
}
