package com.equipe09.Prototype;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
 * Controlleur pour Inscrire un membre à une séance (CU #7)
 * */

public class InscriptionSeance {

	public static void inscriptionSeance(String dateService, String numeroProfessionnel, String numeroMembre,
			String codeService, String commentaire) {
		LocalDate dateServiceValide;
		int numeroProfessionnelValide;
		int numeroMembreValide;
		int codeServiceValide;
		String commentaireValide;
		try {
			dateServiceValide = CentreDeDonnee.stringToLocalDate(dateService, "dd-MM-uuuu");
			numeroProfessionnelValide = Integer.parseInt(numeroProfessionnel);
			numeroMembreValide = Integer.parseInt(numeroMembre);
			codeServiceValide = Integer.parseInt(codeService);
			
			Inscription i = new Inscription();
			i.setCodeService(codeServiceValide);
			i.setCommentaire(commentaire);
			i.setDateService(dateServiceValide);
			i.setNumeroMembre(numeroMembreValide);
			i.setNumeroProfessionnel(numeroProfessionnelValide);
			i.setDateHeureInscription(LocalDateTime.now());
			
			/*Sauvegarder l'enregistrement sur le disque*/
			System.out.println("Le membre a besoin de payer 50$ pour le service. Appuyer sur une touche pour compléter l'inscription...");
			System.in.read();
			/*Informer le membre qu'il est inscrit à la séance/service*/
			System.out.println("Le membre est inscrit à la séance ou au service.");
			
			i.ajouter();
		}
		catch(Exception e) {
			System.out.println("Une des informations soumises n'est pas valide.");
		}
	}
}
